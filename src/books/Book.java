package books;

public class Book {
    private String name;
    private String author;
    private int year;
    private Genre genre;
    private String url;

    Book(String name, String author, int year, Genre genre, String url) {
        this.name = name;
        this.author = author;
        this.year = year;
        this.genre = genre;
        this.url = url;
    }

    String getName() {
        return name;
    }

    String getAuthor() {
        return author;
    }

    int getYear() {
        return year;
    }

    Genre getGenre() {
        return genre;
    }

    public String toString() {
        return name + "{author: " +
                author + ", year of publication: " +
                year + ", genre: " +
                genre + ", url: " +
                url +
                "}";
    }
}