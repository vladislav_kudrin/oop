package books;

import java.util.Scanner;
import java.util.ArrayList;

/**
 * Sorts elements in {@code books} by authors names and years by the insertion sort method.
 * Finds books in {@code books} by {@code genresNumber, authorsName, booksName} and adds results to {@code filteredGenres, filteredAuthors, foundNames}.
 *
 * @author Vladislav
 * @version 1.1
 * @since 02.25.2020
 */
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        ArrayList<Book> filteredGenres = new ArrayList<>();
        ArrayList<Book> filteredAuthors = new ArrayList<>();
        ArrayList<Book> foundNames = new ArrayList<>();
        Book[] books = new Book[5];

        books[0] = new Book("Bioshock: Rapture", "John Shirley", 2011, Genre.FANTASY, "C:\\Books\\Rapture.epub");
        books[1] = new Book("Atlas Shrugged", "Ayn Rend", 1957, Genre.NOVEL, "C:\\Books\\Atlas_Shrugged.epub");
        books[2] = new Book("IT", "Stephen King", 1986, Genre.NOVEL, "C:\\Books\\IT.epub");
        books[3] = new Book("A Study in Scarlet", "Conan Doyle", 1887, Genre.DETECTIVE, "C:\\Books\\Sherlock_Holmes.epub");
        books[4] = new Book("Book", "Author", 0, Genre.NONE, "C:\\Books\\Book.epub");

        System.out.println("Library:");
        output(books);

        authorSort(books);

        System.out.println("\nLibrary sorted by authors:");
        output(books);

        yearSort(books);

        System.out.println("\nLibrary sorted by year of publication:");
        output(books);

        System.out.print("\nAvailable genres:\nDetective\nFantasy\nNovel\nPlease, enter a genre: ");
        genreFilter(books, input.nextLine(), filteredGenres);
        System.out.println("\nBooks filtered by genre:");
        output(filteredGenres);

        System.out.print("\nPlease, enter an author's name: ");
        authorFilter(books, input.nextLine(), filteredAuthors);
        System.out.println("\nBooks filtered by authors:");
        output(filteredAuthors);

        System.out.print("\nPlease, enter a book's name: ");
        nameSearch(books, input.nextLine(), foundNames);
        System.out.println("\nBooks found by name:");
        output(foundNames);
    }

    /**
     * Sorts elements in {@code books} by authors names by the insertion sort method.
     *
     * @param books the one-dimensional array contains elements of Book type.
     */
    private static void authorSort(Book[] books) {
        Book temporary;
        int index;

        for(int queue = 1; queue < books.length; queue++) {
            temporary = books[queue];
            index = queue;

            while(index > 0 && books[index - 1].getAuthor().compareTo(temporary.getAuthor()) >= 0) {
                books[index] = books[index - 1];
                index--;
            }

            books[index] = temporary;
        }
    }

    /**
     * Sorts elements in {@code books} by years of publication by the insertion sort method.
     *
     * @param books the one-dimensional array contains elements of Book type.
     */
    private static void yearSort(Book[] books) {
        Book temporary;
        int index;

        for(int queue = 1; queue < books.length; queue++) {
            temporary = books[queue];
            index = queue;

            while(index > 0 && books[index - 1].getYear() >= temporary.getYear()) {
                books[index] = books[index - 1];
                index--;
            }

            books[index] = temporary;
        }
    }

    /**
     * Finds books in {@code books} by {@code genre} and adds them to {@code filteredGenres}.
     *
     * @param books the one-dimensional array contains elements of Book type.
     * @param genre a genre using to search books in {@code books}.
     * @param filteredGenres a list contains elements of Book type found by {@code genre}.
     */
    private static void genreFilter(Book[] books, String genre, ArrayList<Book> filteredGenres) {
//        Genre genre;
/*
        switch (genresNumber) {
            case 1:
                genre = Genre.DETECTIVE;
                break;
            case 2:
                genre = Genre.FANTASY;
                break;
            case 3:
                genre = Genre.NOVEL;
                break;
            default:
                genre = Genre.NONE;
                break;
        }
*/

        for(Book book : books) {
            if(genre.equals(book.getGenre().getGenresName())) {
                filteredGenres.add(book);
            }
        }
    }

    /**
     * Find books in {@code books} by {@code authorsName} and adds them to {@code filteredAuthors}.
     *
     * @param books the one-dimensional array contains elements of Book type.
     * @param authorsName an author's name using to find books in {@code books} with the similar author.
     * @param filteredAuthors a list contains elements of Book type found by {@code authorsName}.
     */
    private static void authorFilter(Book[] books, String authorsName, ArrayList<Book> filteredAuthors) {
        for(Book book : books) {
            if(authorsName.equalsIgnoreCase(book.getAuthor())) {
                filteredAuthors.add(book);
            }
        }
    }

    /**
     * Finds books in {@code books} by {@code booksName} and adds them to {@code foundNames}.
     *
     * @param books the one-dimensional array contains elements of Book type.
     * @param booksName a book's name using to find books in {@code books} with the similar name.
     * @param foundNames a list contains elements of Book type found by {@code booksName}.
     */
    private static void nameSearch(Book[] books, String booksName, ArrayList<Book> foundNames) {
        for(Book book : books) {
            if(booksName.equalsIgnoreCase(book.getName())) {
                foundNames.add(book);
            }
        }
    }

    /**
     * Outputs values of elements in {@code books}.
     *
     * @param books an one-dimensional array contains elements of Book type.
     */
    private static void output(Book[] books) {
        for(Book book : books) {
            System.out.println(book);
        }
    }

    /**
     * Outputs values of elements in {@code books}.
     *
     * @param books a list contains elements of Book type.
     */
    private static void output(ArrayList<Book> books) {
        for(Book book : books) {
            System.out.println(book);
        }
    }
}