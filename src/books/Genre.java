package books;

public enum Genre {
    DETECTIVE("Detective"),
    FANTASY("Fantasy"),
    NOVEL("Novel"),
    NONE("None");

    private String genresName;

    Genre(String genre) {
        this.genresName = genre;
    }

    String getGenresName() {
        return genresName;
    }
}