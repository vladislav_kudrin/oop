package identifier.search;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Searches identifiers in IdentifierSearch.java by {@code pattern} and adds them to {@code foundIdentifiers}.
 * Creates a list contains non-repeated identifiers and show them.
 *
 * @author Vladislav
 * @version 1.0
 * @since 03.01.2020
 */
public class IdentifierSearch {
    public static void main(String[] args) throws IOException {
        ArrayList<String> foundIdentifiers = new ArrayList<>();

        identifierSearch(foundIdentifiers);

        HashSet<String> uniqueIdentifiers = new HashSet<>(foundIdentifiers);

        showIdentifier(uniqueIdentifiers);
    }

    /**
     * Searches identifiers in IdentifierSearch.java by {@code pattern} and adds them to {@code foundIdentifiers}.
     *
     * @param foundIdentifiers a list for storing found identifiers in IdentifierSearch.java by {@code pattern}.
     * @throws IOException if bufferedReader is null.
     */
    private static void identifierSearch(ArrayList<String> foundIdentifiers) throws IOException{
        String string;
        Pattern pattern = Pattern.compile("[A-Za-z_][A-Za-z_0-9]*");
        Matcher matcher = pattern.matcher("");

        try(BufferedReader bufferedReader = new BufferedReader(new FileReader("src/identifier/search/IdentifierSearch.java"))) {
            while((string = bufferedReader.readLine()) != null) {
                matcher.reset(string);

                while(matcher.find()) {
                    foundIdentifiers.add(matcher.group());
                }
            }
        }
    }

    /**
     * Shows content of {@code uniqueIdentifiers}.
     *
     * @param uniqueIdentifiers a list contains non-repeating identifiers.
     */
    private static void showIdentifier(HashSet<String> uniqueIdentifiers) {
        for(String string : uniqueIdentifiers) {
            System.out.println(string);
        }
    }
}