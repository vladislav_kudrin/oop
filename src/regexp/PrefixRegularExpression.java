package regexp;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.IOException;
import java.io.FileWriter;
import java.util.ArrayList;

/**
 * Searches words in input.txt by {@code pattern}, prints them to the console and saves them to output.txt.
 *
 * @author Vladislav
 * @version 1.0
 * @since 02.29.2020
 */
public class PrefixRegularExpression {
    public static void main(String[] args) throws IOException {
        ArrayList<String> foundWords = new ArrayList<>();

        wordSearch(foundWords);

        saveToFile(foundWords);
    }

    /**
     * Searches words in input.txt by {@code pattern}, prints them to the console and saves them to {@code foundWords}.
     *
     * @param foundWords an ArrayList for storing found words.
     * @throws IOException if {@code bufferReader} is null.
     */
    private static void wordSearch(ArrayList<String> foundWords) throws IOException {
        String string;
        Pattern pattern = Pattern.compile("\\b[Пп]р[еи][а-яё]*");
        Matcher matcher = pattern.matcher("");

        try(BufferedReader bufferedReader = new BufferedReader(new FileReader("src/regexp/input.txt"))) {
            while((string = bufferedReader.readLine()) != null) {
                matcher.reset(string);

                while(matcher.find()) {
                    System.out.println(matcher.group());

                    foundWords.add(matcher.group());
                }
            }
        }
    }

    /**
     * Saves a content of {@code foundWords} to output.txt.
     *
     * @param foundWords the ArrayList contains found words.
     * @throws IOException if {@code foundWords} is empty.
     */
    private static void saveToFile(ArrayList<String> foundWords) throws IOException {
        try (FileWriter fileWriter = new FileWriter("src/regexp/output.txt")) {
            fileWriter.write(String.valueOf(foundWords));
        }
    }
}