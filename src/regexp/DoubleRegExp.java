package regexp;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
 * Searches and prints identifiers in {@code string} by {@code firstPattern, secondPattern}.
 *
 * @author Vladislav
 * @version 1.0
 * @since 02.28.2020
 */
public class DoubleRegExp {
    public static void main(String[] args) {
        String string = "2.5 -5.78 plus + +67 .8 9. +. 23.12e+10";
        String firstPattern = "([+-]?(\\d*\\.\\d+)|(\\d+\\.\\d*))\\W";
        String secondPattern = "([+-]?(\\d*\\.\\d*[eE][+-]?\\d*))";

        System.out.println("First pattern:");
        searchIdentifiers(string, firstPattern);

        System.out.println("\nSecond pattern:");
        searchIdentifiers(string, secondPattern);
    }

    /**
     * Searches and prints identifiers in {@code string} by {@code stringPattern}.
     *
     * @param string the string contains different data.
     * @param stringPattern a regular expression's pattern using to search identifiers in {@code string}.
     */
    private static void searchIdentifiers(String string, String stringPattern) {
        Pattern pattern = Pattern.compile(stringPattern);
        Matcher matcher = pattern.matcher(string);

        while(matcher.find()) {
            System.out.println(matcher.group());
        }
    }
}