package sadsmiles.replace.printers;

public interface IPrinter {
    void print(final String text);
}