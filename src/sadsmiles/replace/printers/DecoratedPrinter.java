package sadsmiles.replace.printers;

public class DecoratedPrinter implements IPrinter{
    public void print(String text) {
        System.out.println("*" + text + "*");
    }
}