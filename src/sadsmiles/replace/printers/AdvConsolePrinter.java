package sadsmiles.replace.printers;

public class AdvConsolePrinter implements IPrinter{
    public void print(final String text) {
        System.out.println(text + " " + text.length());
    }
}