package sadsmiles.replace.printers;

import java.io.FileWriter;
import java.io.IOException;

public class FilePrinter implements IPrinter{
    public void print(String text) {
        try(FileWriter fileWriter = new FileWriter("src\\sadsmiles\\replace\\newText.txt")) {
            fileWriter.write(text);
        }
        catch(IOException e) {
            System.out.println("File not created!");
        }
    }
}
