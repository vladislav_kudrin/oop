package sadsmiles.replace.printers;

public class ConsolePrinter implements IPrinter {
    public void print(final String text) {
        System.out.println(text);
    }
}