package sadsmiles.replace;

import sadsmiles.replace.printers.IPrinter;
import sadsmiles.replace.readers.IReader;

class Replacer {
    private IReader reader;
    private IPrinter printer;

    Replacer(IReader reader, IPrinter printer) {
        this.reader = reader;
        this.printer = printer;
    }

    void replacer() {
        final String text = reader.read();
        final  String replacedText = text.replaceAll(":\\(", ":)");
        printer.print(replacedText);
    }
}
