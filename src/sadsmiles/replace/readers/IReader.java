package sadsmiles.replace.readers;

public interface IReader {
    String read();
}