package sadsmiles.replace.readers;

import java.util.List;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.io.IOException;

public class FileReader implements IReader{
    public String read() {
        String text = "";

        try {
            List<String> list = Files.readAllLines(Paths.get("src\\sadsmiles\\replace\\text.txt"));
            text = list.toString();
        }
        catch(IOException e) {
            System.out.println("File not found!");
        }

        return text;
    }
}
