package sadsmiles.replace;

import sadsmiles.replace.printers.*;
import sadsmiles.replace.readers.FileReader;
import sadsmiles.replace.readers.IReader;
import sadsmiles.replace.readers.PredefinedReader;

/**
 * Reads a text from console, replace all sad-smiles to smiles and prints a result to console.
 * Reads a text from text.txt, replace all sad-smiles to smiles and saves a result to newText.txt.
 *
 * @author Vladislav
 * @version 1.0
 * @since 03.09.2020
 */
public class Main {
    public static void main(String[] args) {
        IReader reader = new PredefinedReader(":(");
        IReader fileReader = new FileReader();
        IPrinter printer = new ConsolePrinter();
        IPrinter advPrinter = new AdvConsolePrinter();
        IPrinter decoratedPrinter = new DecoratedPrinter();
        IPrinter filePrinter = new FilePrinter();
        Replacer replacer = new Replacer(reader, printer);
        Replacer advReplacer = new Replacer(reader, advPrinter);
        Replacer decoratedReplacer = new Replacer(reader, decoratedPrinter);
        Replacer fileReplacer = new Replacer(fileReader, filePrinter);

        replacer.replacer();

        advReplacer.replacer();

        decoratedReplacer.replacer();

        fileReplacer.replacer();
    }
}