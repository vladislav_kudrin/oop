package obfuscator.mini;

/*
Multi comment.
*/
//Single comment
import java.io.IOException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

/**
 * Reads code file, delete all comments, spaces and saves changes to a new file.
 *
 * @author Vladislav
 * @version 1.0
 * @since  03.10.2020
 */
public class ObfuscatorMini {
    public static void main(String[] args) {
        String code = "";
        String fileName;
        String path = "src/obfuscator/mini/ObfuscatorMini.java", newPath;

        code = readFile(code, path);

        code = deleteComments(code);

        code = deleteNewLines(code);

        code = deleteSpaces(code);

        fileName = getFileName(path);

        code = rename(fileName, code);

        newPath = createNewPath(path);

        saveFile(newPath, code);
    }

    /**
     * Constructor for a test.
     */
    public ObfuscatorMini() {
    }

    /**
     * Gets {@code fileName} from {@code path}.
     *
     * @param path a path to a code file.
     * @return a code file's name.
     */
    private static String getFileName(String path) {
        String[] splitPath = path.split("/");
        String[] fileName = splitPath[splitPath.length - 1].split("\\.");

        return fileName[0];
    }

    /**
     * Reads code file from {@code path} and saves it to {@code code}.
     *
     * @param code a program's code.
     * @param path a path to a code file.
     */
    private static String readFile(String code, String path) {
        String string;

        try(BufferedReader bufferedReader = new BufferedReader(new FileReader(path))) {
            while ((string = bufferedReader.readLine()) != null) {
                code = code.concat(string + "\n");
            }
        }
        catch(IOException e) {
            System.out.println("File not found!");
        }

        return code;
    }

    /**
     * Replaces all spaces to just one space in {@code code} and returns a result.
     *
     * @param code a program's code.
     * @return a program's code with replaced spaces.
     */
    private static String deleteSpaces(String code) {
        return code.replaceAll("\\s+", " ");
    }

    /**
     * Deletes all comments in {@code code} and returns result.
     *
     * @param code a program's code.
     * @return a program's code with deleted comments.
     */
    private static String deleteComments(String code) {
        return code.replaceAll("(/\\*(.|[\\n\\r])*?\\*/)|([^\"()\\[\\]]//.*[\\n\\r])", "");
    }

    /**
     * Deletes all newLine characters in {@code code} and returns a result.
     *
     * @param code a program's code.
     * @return a program's code with deleted newLine characters.
     */
    private static String deleteNewLines(String code) {
        return code.replaceAll("\\n", "");
    }

    /**
     * Rename a class-name and a constructor-name in {@code code} and returns a result.
     *
     * @param fileName a code file's name.
     * @param code a program's code.
     * @return a program's code with renamed a class-name and a constructor-name.
     */
    private static String rename(String fileName, String code) {
        code = code.replaceAll("[^/\"]" + fileName, " New" + fileName);
        return code;
    }

    /**
     * Generates a new code file's path.
     *
     * @param path a code file's path.
     * @return a new code file's path.
     */
    private static String createNewPath(String path) {
        String newPath = "";

        String[] splitPath = path.split("/");
        splitPath[splitPath.length - 1] = "New" + splitPath[splitPath.length - 1];

        for(String string : splitPath) {
            newPath = newPath.concat(string + "/");
        }

        return newPath;
    }

    /**
     * Creates a new code file with a new path.
     *
     * @param newPath a new code file's path.
     * @param code a program's code.
     */
    private static void saveFile(String newPath, String code) {
        try(BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(newPath))) {
            bufferedWriter.write(code);
        }
        catch(IOException e) {
            System.out.println("File not created!");
        }
    }
    //Single comment.
    /*
    Multi comment.
    */
}